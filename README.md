# Intro à la notion de haute disponibilité sous k8s

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectif
L'objectif est d'avoir un apperçu des différents scénarios de hautes disponibilité d'un cluter Kuberntes. La finalité été de rendre le services kubernetes résiliante aux défaillances techniques.

Nous verrons ainsi ce qu'est : 
 1. La haute disponibilité au sein d'un cluster k8s
 2. La notion de haute disponibilité pour un Control Plane
 3. La notion de Stack etcd
 4. La notion de etcd Externe

# Explication / Présentation

## 1. La haute disponibilité au sein d'un cluster Kubernetes
La haute disponibilité, dans le contexte de Kubernetes, signifie que le cluster Kubernetes lui-même, et non seulement les applications conteneurisées qu'il héberge, est conçu pour rester opérationnel malgré les défaillances possibles de certains de ses composants. Pour y parvenir, nous avons besoin de multiples nœuds du plan de contrôle, ce qui assure la redondance et la continuité des services.

## 2. La notion de haute disponibilité pour un Control Plane
La haute disponibilité pour le plan de contrôle de Kubernetes implique la présence de plusieurs nœuds de plan de contrôle, chacun exécutant les mêmes composants critiques tels que le kube-api-server. Lorsque nous avons plusieurs instances du kube-api-server, nous avons besoin d'un équilibreur de charge pour gérer la communication avec ces instances. Ainsi, les interactions avec Kubernetes via des outils comme kubectl se font via un équilibreur de charge, qui distribue le trafic vers les différentes instances du kube-api-server. De même, les nœuds de travail (workers), exécutant les kubelets, communiquent avec l'API Kubernetes en utilisant cet équilibreur de charge, garantissant ainsi une communication fiable même si certains nœuds du plan de contrôle deviennent indisponibles.

## 3. La notion de Stack etcd
Un modèle courant pour gérer la haute disponibilité de l'etcd dans Kubernetes est le modèle **"stack etcd"**. Dans ce modèle, etcd fonctionne sur les mêmes nœuds que les autres composants du plan de contrôle. C'est le modèle utilisé par kubeadm, un outil courant pour installer Kubernetes. Lorsque nous utilisons kubeadm pour configurer notre cluster, il utilise cette méthodologie, où chaque nœud du plan de contrôle exécute une instance d'etcd en plus des autres composants du plan de contrôle. Dans une configuration de haute disponibilité, chaque nœud du plan de contrôle aurait sa propre instance d'etcd, assurant ainsi la redondance et la résilience des données de configuration.

## 4. La notion de etcd Externe
Un autre modèle de gestion de l'etcd pour la haute disponibilité est le modèle "etcd externe". Contrairement au modèle **"stack etcd"**, où **etcd** fonctionne sur les mêmes nœuds que les composants du plan de contrôle, dans le modèle etcd externe, etcd est exécuté sur des nœuds distincts. Cela signifie que dans une configuration de haute disponibilité, nous avons plusieurs nœuds etcd externes, séparés des nœuds exécutant les composants du plan de contrôle de Kubernetes. Ce modèle permet une plus grande flexibilité et une isolation des services, car les nœuds etcd externes peuvent être dimensionnés et gérés indépendamment des nœuds du plan de contrôle. Ainsi, nous pouvons avoir un nombre quelconque d'instances du plan de contrôle Kubernetes et un nombre quelconque de nœuds etcd, assurant une robustesse et une résilience accrues du cluster Kubernetes.

# Conclusion 
Nous avons exploré la haute disponibilité dans Kubernetes, essentielle pour la résilience des services en production. 
Nous avons défini ce concept pour un cluster Kubernetes et l'importance des multiples nœuds du plan de contrôle. 
Nous avons également discuté de deux modèles pour gérer les instances d'etcd : le modèle **stacked etcd**, où **etcd** partage les nœuds avec les composants du plan de contrôle, et le modèle **etcd externe**, où **etcd** fonctionne sur des nœuds distincts. Ces approches assurent la robustesse et la continuité des services Kubernetes, même en cas de défaillances.